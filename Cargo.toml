[package]
name = "mdcat"
description = "cat for markdown: Show markdown documents in terminals"
readme = "README.md"
homepage = "https://codeberg.org/flausch/mdcat"
repository = "https://codeberg.org/flausch/mdcat"
documentation = "https://docs.rs/mdcat"
keywords = ["markdown", "less", "cat"]
version = "0.27.1"
categories = ["command-line-utilities", "text-processing"]
license = "MPL-2.0"
authors = ["Sebastian Wiesner <sebastian@swsnr.de>"]
edition = "2021"

[features]
default = ["tree_magic_mini"]

[dependencies]
ansi_term = "^0.12"
anyhow = "^1"
base64 = "^0.13"
clap = { version = "3.1.8", default-features = false, features = ["std", "cargo"] }
env_proxy = "^0.4"
fehler = "^1"
gethostname = "^0.2"
image = "0.24.0"
mime = "^0.3"
pulldown-cmark = { version = "0.9.1", default-features = false, features = ['simd'] }
shell-words = "^1"
tracing = { version = "0.1.30", features = ["release_max_level_warn"] }
tracing-subscriber = { version = "0.3.8", features = ["env-filter"] }
ureq = { version = "2.4.0", features = ["tls", "native-certs"] }
url = "^2.2"
tree_magic_mini = {version = "3.0.3", optional = true}

[dependencies.syntect]
version = "4.6.0"
default-features = false
features = ["parsing", "assets", "dump-load", "regex-fancy"]

[target.'cfg(unix)'.dependencies]
libc = "^0.2"

[target.'cfg(windows)'.dependencies]
terminal_size = "^0.1"

[dev-dependencies]
pretty_assertions = "1.2.1"
lazy_static = "^1.4"
test-generator = "^0.3"

[build-dependencies]
# To generate completions during build
clap = { version = "3.1.8", default-features = false, features = ["std", "cargo"] }
clap_complete = "3.1.1"

[profile.release]
# Enable LTO for release builds; makes the binary a lot smaller
lto = true

[package.metadata.release]
allow-branch = ["main"]
pre-release-commit-message = "Release {{version}}"
post-release-commit-message = "Bump version to {{next_version}}"
tag-prefix = "mdcat-"
tag-name = "{{prefix}}{{version}}"
tag-message = "mdcat {{version}}"
pre-release-replacements = [
    # Update version and release date in manpage
    { file="mdcat.1.adoc", search="(?m)^:revnumber:.*$", replace = ":revnumber: {{version}}" },
    { file="mdcat.1.adoc", search="(?m)^:revdate:.*$", replace = ":revdate: {{date}}" },
    # Update the changelog
    { file="CHANGELOG.md", search="## \\[Unreleased\\]", replace = "## [Unreleased]\n\n## [{{version}}] – {{date}}", exactly = 1 },
    { file="CHANGELOG.md", search="HEAD", replace = "{{tag_name}}", exactly = 1 },
    { file="CHANGELOG.md", search="\\[Unreleased\\]: ", replace = "[Unreleased]: https://codeberg.org/flausch/mdcat/compare/{{tag_name}}...HEAD\n[{{version}}]: ", exactly = 1 },
]
